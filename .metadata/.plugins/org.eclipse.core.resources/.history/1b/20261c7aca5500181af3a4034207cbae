package tests;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.jar.Manifest;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Locke
 * @version 1.0
 */

public class manifestGeneratorTests {
	/**
	 * Test 0: Declare a manifestGenerator object
	 */
	manifestGenerator generator;
	
	/**
	 * Test 1: Create a manifestGenerator object
	 */
	@Before @Test public void createGenerator() {
		generator = new manifestGenerator();
	}
	

	/**
	 * Test 2: Given dummy item list, make a manifest that contains an ordinary truck
	 */
	@Test public void addTrucks() {
		Stock storeStock=new Stock();
		Item storeItem = new Item("rice",2,4,100,500,null,0);
		storeStock.add(storeItem);
		generator.generate(storeStock);
		
		Manifest manifest = generator.get();
		ArrayList<Truck> trucks = manifest.get();
		ordinaryTruck truck = new ordinaryTruck();
		assertEquals("Could not make a manifest with an ordinary truck",
				truck.getClass(),trucks.get(0).getClass());
	}
	
	/**
	 * Test 3: Given a dummy item list with a cold item, make a manifest with a cold truck
	 */
	@Test public void addColdTruck() {
		Stock storeStock=new Stock();
		Item storeItem = new Item("ice",2,4,100,500,-1,0);
		storeStock.add(storeItem);
		generator.generate(storeStock);
		
		Manifest manifest = generator.get();
		ArrayList<Truck> trucks = manifest.get();
		refrigeratedTruck truck = new refrigeratedTruck();
		assertEquals("Could not make a manifest with a cold truck",
				truck.getClass(),trucks.get(0).getClass());
	}

	/**
	 * Test 4: correctly get the right amount of items onto a truck
	 */
	@Test public void addItemsToTruck() {
		//Create a stock object with items
		Stock storeStock=new Stock();
		Item storeItem = new Item("ice",2,4,100,500,-1,0);
		storeStock.add(storeItem);
		//Make a manifest
		generator.generate(storeStock);
		//Get the manifest
		Manifest manifest = generator.get();
		//get from the manifest it's arraylist of trucks.Then, get the first truck. Then, get its stock object.
		//then get the arraylist of item objects
		Item testItem = manifest.get().get(0).get().get().get(0);
		//Assert that the quantity of the item is 500
		assertEquals("Could not get the correct quantity of items",
				500,testItem.getQuantity());
	}
	
	/**
	 * Test 5: Correctly fill up a truck when there's too many items for just one
	 */
	@Test public void topUpATruck() {
		//create a stock object with items
		Stock storeStock=new Stock();
		Item storeItem = new Item("ice",2,4,100,500,-1,0);
		Item storeItem2 = new Item("colderice",2,4,100,500,-10,0);
		storeStock.add(storeItem);
		storeStock.add(storeItem2);
		
		//generate the manifest, get it, and get the first truck
		generator.generate(storeStock);
		Manifest mani=generator.get();
		refrigeratedTruck truck = mani.get().get(0);
		
		//assert that the trucks capacity is 0
		assertEquals("Did not correctly fill trucks up",
				0,truck.getCapacity());
	}
	
	/** 
	 * Test 6: When items overflow from one truck to another, make sure it's done right
	 * 
	 */
	@Test public void overflowToSecondTruck() {
		//create a stock object with items
		Stock storeStock=new Stock();
		Item storeItem = new Item("ice",2,4,100,500,-1,0);
		Item storeItem2 = new Item("colderice",2,4,100,500,-10,0);
		storeStock.add(storeItem);
		storeStock.add(storeItem2);
		//generate the manifest, get it, and get the second truck
		generator.generate(storeStock);
		Manifest mani=generator.get();
		refrigeratedTruck truck = mani.get().get(1);
		//we are loading 1000 cold items. Fridge trucks have a capacity for 800. 
		//Therefore assert the second truck has 1000-800=200 items onboard =  600 remaining capacity
		assertEquals("Did not correctly overflow into second truck",
				600,truck.getCapacity());
	}
	/**
	 * Test 7: Fill up a cold truck, leaving only dry goods remaining (ordinary truck)
	 */
	@Test public void multipleTruckTypes() {
		Stock storeStock=new Stock();
		Item storeItem = new Item("ice",2,4,100,500,-1,0);
		Item storeItem2 = new Item("rice",2,4,100,500,null,0);
		storeStock.add(storeItem);
		storeStock.add(storeItem2);
		
		//generate the manifest, get it, and get the second truck
		generator.generate(storeStock);
		Manifest mani=generator.get();
		Truck truck1 = mani.get().get(1);
		//make a new truck object
		ordinaryTruck truck2 = new ordinaryTruck();
		//assert that the truck has the correct type
		assertEquals("Second truck was not a dry truck",
				truck2.getClass(),truck1.getClass());
		
	}
	
	/**
	 * Test 8: Same as test 7, but the items are not presorted. 
	 */
	@Test public void sortBeforeLoading() {
		Stock storeStock=new Stock();
		Item storeItem = new Item("ice",2,4,100,500,-1,0);
		Item storeItem2 = new Item("rice",2,4,100,500,null,0);
		storeStock.add(storeItem2);
		storeStock.add(storeItem);
		
		//generate the manifest, get it, and get the second truck
		generator.generate(storeStock);
		Manifest mani=generator.get();
		Truck truck1 = mani.get().get(1);
		//make a new truck object
		ordinaryTruck truck2 = new ordinaryTruck();
		//assert that the truck has the correct type
		assertEquals("Second truck was not a dry truck",
				truck2.getClass(),truck1.getClass());
	}
	
	/**
	 * Test 9, can correctly make multiple of each type of truck
	 */
	@Test public void lotsandlotsoftrucks() {
		//create stock and give it items
		Stock stock = new Stock();
		Item item = new Item("ice",2,4,100,900,-1,0);
	}
}
